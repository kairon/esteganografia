#include <iostream>
#include <string>  
#include <fstream>
#include <sstream>
#include "pic.hpp"

using namespace std;

Pic::Pic(){

}

void Pic::open(string file_path){

	fstream file;
	file.open(file_path.c_str(), fstream::in | fstream::binary);

    /* File was not open */
    if(!file.is_open()){
        cerr << "Arquivo não disponível." << endl;
        return;
    }

    string line = "";

    /* Get check the magic number */
    getline(file, line);
   	if(line.compare(this->getVersion()) != 0){
   		cerr << " Versão de imagem inválida." << endl;
   		return;
   	}
   	cout << "Número mágico :  " << line << endl;

    /* Get and print the comment */
    getline(file, line);
    cout << "Comentário :  " << line << endl;

    stringstream ss;
    int width, height;

    /* Get the width and height */
    getline(file, line);
    ss << line;
    ss >> width >> height;

    this->setWidth(width);
    this->setHeight(height);

    cout << "Largura :  " << width << endl;
    cout << "Altura :  " << height << endl;


   	this->setPixels(width*height*this->getPixelDensity());

   	// Get max gray
    getline(file, line);

    int pos = 0;
    char pixel;
    
    while(file.get(pixel)){
    	this->setPixel(pos, pixel);
    	pos++;
    }
    file.close();
}

// Virtual sem implemento
void Pic::read(){

}

// Virtual sem implemento
void Pic::save(){

}

void Pic::setPixels(int total){
	this->pixels = new char[total];
}

void Pic::setPixel(int pos, char pixel){
	this->pixels[pos] = pixel;
}

void Pic::setVersion(string version){
	this->version = version;
}

void Pic::setWidth(int width){
	this->width = width;
}

void Pic::setHeight(int height){
	this->height = height;
}

void Pic::setPixelDensity(int pixel_density){
	this->pixel_density = pixel_density;
}

int Pic::getPixelDensity(){
	return this->pixel_density;
}
char Pic::getPixel(int pos){
	return this->pixels[pos];
}

int Pic::getWidth(){
	return this->width;
}
int Pic::getHeight(){
	return this->height;
}

string Pic::getVersion(){
	return this->version;
}
