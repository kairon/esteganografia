#include <iostream> 
#include <string>
#include <fstream>
#include <sstream>
#include "pgm.hpp"

PGM::PGM() {
	this->setVersion("P5");
	this->setPixelDensity(1);
}

void PGM::open(string file_path){
	Pic::open(file_path);

	int start;
	cout << "Posição de inicial da mensagem :  ";
	cin >> start;
	this->setStartPosition(start);
	this->read();
	this->save();
}

void PGM::read(){
	
    int count = 1, pos;
    char character = 0;
   
   	for(pos=this->getStartPosition(); pos < this->getWidth() * this->getHeight(); pos++){
   		character <<= 1;
        character |= this->getPixel(pos) & 0x01;
        // Is it the least significant bit ? 
        if(count == 8){
            // Is it the end of the message ?
            if(!(character ^ '#')){
                break;
            }
            // Print out the message 
           	//cout <<  character;
           	this->setMessage(character);
            // Reset the message 
            count = 0;
            character = 0;
        }
        count++;
   	}

   	cout << this->getMessage() << endl;
}

void PGM::save(){
	fstream outfile;
	string file_path;
	cout << "Arquivo de saída (.txt):" << endl;
	cin >> file_path;
  outfile.open(file_path.c_str(), fstream::out);
  outfile << this->getMessage();
  outfile.close();
  cout << "mensagem salva com êxito." << endl;
}

void PGM::setMessage(char character){
	stringstream ss;
	string message;
	ss << character;
	ss >> message;
	cout << message;
	this->message += message;
}

void PGM::setStartPosition(int start){
	this->start_position = start;
}

int PGM::getStartPosition(){
	return this->start_position;
}

string PGM::getMessage(){
	return this->message;
}