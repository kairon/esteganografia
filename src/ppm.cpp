#include <iostream> // std::cout
#include <string>   // std::
#include <fstream>  // std::fstream
#include <sstream>  // std::stringstrea
#include "ppm.hpp"

PPM::PPM() {
	Pic::setVersion("P6");
  this->setPixelDensity(3);
}

void PPM::open(string file_path){
	Pic::open(file_path);

  char filter;
  string file_name;
  cout << "Filtro a aplicar (rgb) : ";
  cin >> filter;
  cout << "Título do arquivo de saída (.ppm): ";
  cin >> file_name;
  this->applyFilter(filter, file_name);
}

void PPM::applyFilter(char filter, string file_path){
  int pos, pos_rgb, color;
  fstream outfile;

  outfile.open(file_path.c_str(), fstream::out);

  outfile << "P6" << endl
          << "#EP 1 ORIENTACAO A OBJETOS 2016.1" << endl
          << this->getWidth() << " " << this->getHeight() << endl
          << "255" << endl;

  unsigned char rbg[3];

  switch(filter){
    case 'r':
      color = 0;
      break;
    case 'g':
      color = 1;
      break;
    case 'b':
      color = 2;
      break;
    default:
      cerr << endl << "Opção indisponível." << endl;
      return;
  }

  for(pos=0; pos < this->getWidth() * this->getHeight()*this->getPixelDensity(); pos += pos_rgb){
    for(pos_rgb = 0; pos_rgb < 3; pos_rgb++){
      // Apply the filter and save to the file.
      if(pos_rgb == color){
        rbg[pos_rgb] = 0;
       }
       else{
        rbg[pos_rgb] = (unsigned char)this->getPixel(pos+pos_rgb);
       }
       outfile << rbg[pos_rgb];
      
    }
  }
  outfile.close();
  cout << "Imagem salva com êxito." << endl;
}