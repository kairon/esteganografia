#include <iostream> // std::cout
#include <string>   // std::
#include <fstream>  // std::fstream
#include <sstream>  // std::stringstream
#include "pic.hpp"
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

int main() {

	int option;
	cout << "1 >> Decifrar imagem PGM " << endl;
	cout << "2 >> Decifrar imagem PPM " << endl;
	cout << "3 >> Sair" << endl;
	cout << "Opção: ";
	cin >> option;

	Pic * image;
	PGM pgm;
	PPM ppm;

	switch(option){
		case 1:
			image = &pgm;
			break;
		case 2:
			image = &ppm;
			break;
		case 3:
			return 0;
		default:
			cerr << "Opção indisponível.";
			return 0;
	}

	string file_path;
	cout << "Título da imagem (ex.: world.[ppm | pgm]): ";
	cin  >> file_path;

	image->open(file_path);

	return 0;
}
