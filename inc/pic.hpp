#ifndef PIC_HPP
#define PIC_HPP


using namespace std;

class Pic { 
	private:
		int width, height, max_grey, pixel_density;
		char * pixels;
		string version;

	public:
		Pic();	// Construtor
		virtual void open(string file);
		virtual void save();
		virtual void read();
		void close(string file);
		void setWidth(int width);
		void setHeight(int height);
		void setVersion(string version);
		void setPixels(int total); 			// To Allocate memory
		void setPixel(int pos, char pixel); // To build the
		void setPixelDensity(int pixel_density);
		int getPixelDensity();
		int getWidth();
		int getHeight();
		void setMaxGrey(int max_grey);
		void setMaxGrey();
		char getPixel(int pos);
		string getVersion();
		

};

#endif

