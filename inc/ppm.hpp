#ifndef PPM_H
#define PPM_H

#include "pic.hpp"

class PPM : public Pic {

	public:
		PPM();
		//~PPM();
		void open(string file_path);
		void applyFilter(char filter, string file_path);


};
#endif
