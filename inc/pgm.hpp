#ifndef PGM_H 
#define PGM_H

#include "pic.hpp"

class PGM : public Pic {
	private:
		int start_position; // Posição de início da mensagem
		string message; // Mensagem escondida
	public:
		PGM();
		void read();
		void open(string file_path);
		void save();
		void setStartPosition(int start);
		void setMessage(char message);
		int getStartPosition();
		string getMessage();

};
#endif