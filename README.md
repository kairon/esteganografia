<b><h1>README</h1></b>

<b>INCLUSO:</b>

Estrutura de pasta

Todos os arquivos fontes (.cpp, .hh, .cpp, .h)

Makefile

Imagens de teste


<b>OBTENÇÃO:</b>


O programa pode ser obtido através da clonagem do repositório, pelo comando:


<pre>
git clone https://gitlab.com/ikairon7/esteganografia.git
</pre>


<b>EXECUÇÃO</b>

Para utilizar o programa, deve-se compilá-lo, usando o make, pelo comando:
<pre>
make
</pre>

E então, para rodá-lo, usa-se o comando

<pre>
make run
</pre>

<b>USO</b>

Ao execultar o programa, aparecerá o menu de opções, bastando apenas seguí-lo para realizar as ações desejadas.

<b>OUTROS</b>

Há imagens de teste na pasta "doc". Na execução do programa, basta informar o caminho do arquivo. Exemplo:

<pre>
doc/lena.pgm
</pre>

Para limpar a pasta de objetos, use o comando:
<pre>
make clean
</pre>